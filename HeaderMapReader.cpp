#include "HeaderMapTypes.h"
#include "argh.h"
#include <filesystem>
#include <fstream>
#include <iostream>

namespace fs = std::filesystem;
bool verbose = false;

/** Swaps between big/little endian
    https://stackoverflow.com/questions/2182002/convert-big-endian-to-little-endian-in-c-without-using-provided-func
*/
std::uint16_t swapEndian (std::uint16_t num) noexcept
{
    return (num >> 8) | (num << 8);
}

/** Swaps between big/little endian */
std::uint32_t swapEndian (std::uint32_t num) noexcept
{
    return ((num >> 24) & 0xff) | // move byte 3 to byte 0
           ((num << 8) & 0xff0000) | // move byte 1 to byte 2
           ((num >> 8) & 0xff00) | // move byte 2 to byte 1
           ((num << 24) & 0xff000000); // byte 0 to byte 3
}

/** Swaps the endianness of the HMapHeader */
clang::HMapHeader swapHeaderEndianness (clang::HMapHeader header)
{
    header.Magic = swapEndian (header.Magic);
    header.Version = swapEndian (header.Version);
    header.Reserved = swapEndian (header.Reserved);
    header.StringsOffset = swapEndian (header.StringsOffset);
    header.NumEntries = swapEndian (header.NumEntries);
    header.NumBuckets = swapEndian (header.NumBuckets);
    header.MaxValueLength = swapEndian (header.MaxValueLength);

    return header;
}

/** Loads hmap header from the data buffer */
clang::HMapHeader getHeader (std::vector<std::byte>& data)
{
    auto header = clang::HMapHeader();
    header = *reinterpret_cast<clang::HMapHeader*> (data.data());
    header = swapHeaderEndianness (header);

    if (verbose)
    {
        std::cout << "[Header]\n";
        std::cout << "  magic: " << header.Magic << "\n";
        std::cout << "  version: " << header.Version << "\n";
        std::cout << "  strings offset: " << header.StringsOffset << "\n";
        std::cout << "  num entries: " << header.NumEntries << "\n";
        std::cout << "  num buckets: " << header.NumBuckets << "\n";
        std::cout << "  max value length: " << header.MaxValueLength << "\n\n";
    }

    return header;
}

/** Reads the file into a vector of bytes */
std::vector<std::byte> readFile (fs::path hmapFile) noexcept
{
    std::vector<std::byte> data;

    if (auto stream = std::ifstream (hmapFile))
    {
        stream.seekg (0, std::ios::end);
        const auto size = stream.tellg();
        stream.seekg (0, std::ios::beg);
        data.resize (size);

        if (! stream.read ((char*) data.data(), size))
        {
            std::cout << "Failed to read file\n";
        }

        if (verbose)
            std::cout << "[x] " << data.size() << " bytes loaded OK.\n\n";
    }

    return data;
}

/** Parses the header map file */
void parseHeaderMap (fs::path hmapFile)
{
    auto isHmapFile = [] (clang::HMapHeader header) -> bool {
        return header.Magic == (std::uint32_t) clang::HMAP_HeaderMagicNumber;
    };

    auto data = readFile (hmapFile);
    auto header = getHeader (data);

    if (! isHmapFile (header))
    {
        std::cout << "File is not .hmap format\n";
        return;
    }

    auto offset = sizeof (clang::HMapHeader);
    std::vector<clang::HMapBucket> buckets (header.NumBuckets);

    // Read buckets into memory
    for (int i = 0; i < header.NumBuckets; ++i)
    {
        buckets[i] = *reinterpret_cast<clang::HMapBucket*> (data.data() + offset + i * sizeof (clang::HMapBucket));
        buckets[i].Key = swapEndian (buckets[i].Key);
        buckets[i].Prefix = swapEndian (buckets[i].Prefix);
        buckets[i].Suffix = swapEndian (buckets[i].Suffix);
    }

    for (auto const& b : buckets)
    {
        if (b.Key > 0)
        {
            const char* stringArray = (const char*) data.data() + header.StringsOffset;
            const char* key = stringArray + b.Key;
            const char* prefix = stringArray + b.Prefix;
            const char* suffix = stringArray + b.Suffix;
            std::cout << "[Entry]\n";
            std::cout << "  Key: " << key << "\n";
            std::cout << "  Prefix: " << prefix << "\n";
            std::cout << "  Suffix: " << suffix << "\n";
        }
    }
}

int main (int, char* argv[])
{
    auto cmdl = argh::parser (argv);

    if (cmdl[{ "-v", "--verbose" }])
        verbose = true;

    if (cmdl.size() > 1)
    {
        auto source = fs::path (cmdl[1]);
        if (fs::exists (source) && fs::is_regular_file (source))
        {
            parseHeaderMap (source);
            return EXIT_SUCCESS;
        }
        else
        {
            std::cout << "file does not exist!\n";
            return EXIT_FAILURE;
        }
    }

    std::cout << "Usage: " << cmdl[0] << " <hmap-file>\n";
    return EXIT_SUCCESS;
}